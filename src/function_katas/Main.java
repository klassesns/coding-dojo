package function_katas;

import function_katas.bauernmultiplikation.Bauernmultiplikation;
import function_katas.galgenmann.Galgenmann;

public class Main {

	public static void main(String[] args) {
		// Starte Galgenmann
		Galgenmann.start();

		// Start Bauernmultiplikation
		Bauernmultiplikation.start();
	}

}
